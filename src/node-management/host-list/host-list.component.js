/*global angular */
'use strict';

// 3rd party
var async = require('async');

/** @enum {string} */
var STATES = {
    NOT_INSTALLED: 'HOST.NOT_INSTALLED',
    RUNNING: 'HOST.RUNNING',
    DOWN: 'HOST.DOWN',
    PENDING: 'HOST.PENDING'
};

angular.module('bitcraft-host.list').
    component('bitcraftHostList', {
        templateUrl: 'js/node-management/host-list/host-list.template.html',
        controller: [
            'Notification',
            function HostListController(Notification) {
                var i;
                var self = this;

                self.selectedDistribution = {};
                self.distributionList = [];

                function init() {
                    async.forEach(self.hostList,
                        function (item, next) {
                            self.checkHttpInstall(item, function (err) {
                                self.checkHttpState(item);
                                self.updateNodeCount(item);
                                next(err);
                            });
                        },
                        /** @param {string} err */
                        function (err) {
                            if (err) {
                                Notification.error({
                                    message: 'Failed to initialize hosts (' + err + ')'
                                });
                            }
                        });
                }

                /**
                 * @param {string} id
                 * @returns {HostInfo|undefined}
                 */
                function getHostById(id) {
                    for (i = 0; i < self.hostList.length; i += 1) {
                        if (self.hostList[i]._id === id) {
                            return self.hostList[i];
                        }
                    }
                }

                self.$onChanges = function (changesObj) {
                    if (changesObj.hostList) {
                        self.hostList = angular.copy(changesObj.hostList.currentValue);
                        if (!changesObj.hostList.isFirstChange() || self.hostList !== undefined) {
                            init();
                        }
                    }
                    if (changesObj.distributionList) {
                        self.distributionList = angular.copy(
                            changesObj.distributionList.currentValue
                        );
                        if (self.distributionList && self.distributionList.length > 0) {
                            self.selectedDistribution = self.distributionList[0];
                        }
                    }
                };

                /**
                 * @param {HostInfo} item
                 * @param {function(string=)} callback
                 */
                self.checkHttpInstall = function (item, callback) {
                    if (item.category === 'http') {
                        item.httpInstalled = false;
                        item.state = STATES.NOT_INSTALLED;
                        self.onCheckHttpInstall({hostInfo: item}).then(
                            function (data) {
                                item.httpInstalled = data.installed;
                                callback();
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to check http state (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                                callback(resp.data);
                            }
                        );
                    }
                };

                /** @param {HostInfo} item */
                self.checkHttpState = function (item) {
                    if (item.httpInstalled) {
                        item.state = STATES.PENDING;
                        self.onCheckHttpState({hostInfo: item}).then(
                            function (data) {
                                item.state = data.running ? STATES.RUNNING : STATES.DOWN;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to check state (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                            }
                        );
                    }
                };

                /** @param {HostInfo} item */
                self.updateNodeCount = function (item) {
                    if (item.category === 'agent') {
                        self.onGetNodeCount({hostInfo: item}).then(
                            function (data) {
                                item.nodeCount = data.nodeCount;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to update node count (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                            }
                        );
                    }
                };

                /**
                 * @param {DistributionInfo} data
                 * @param {HostInfo} host
                 */
                self.installHttp = function (data, host) {
                    self.isOperationInProgress = true;
                    self.onInstallHttp({
                        distributionId: data._id,
                        hostId: host._id,
                        callback: function () {
                            self.isOperationInProgress = false;
                            self.checkHttpInstall(host, function () {
                                host.state = STATES.DOWN;
                            });
                        }
                    });
                };

                /**
                 * Returns true if the host can be installed
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canInstall = function (data) {
                    return true;
                };

                /**
                 * Returns true if the host can be started
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canStart = function (data) {
                    var host = getHostById(data._id);
                    if (!host) {
                        return false;
                    }

                    return host.httpInstalled && host.state === STATES.DOWN;
                };

                /**
                 * Returns true if the host can be stopped
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canStop = function (data) {
                    var host = getHostById(data._id);
                    if (!host) {
                        return false;
                    }

                    return host.httpInstalled && host.state === STATES.RUNNING;
                };

                /** @param {HostInfo} data */
                self.reinstallSentinel = function (host) {
                    self.isOperationInProgress = true;
                    self.onReinstallSentinel({
                        hostInfo: host,
                        callback: function () {
                            self.isOperationInProgress = false;
                        }
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.startHttp = function (hostInfo) {
                    self.onStartHttp({
                        hostInfo: hostInfo,
                        callback: function () {
                            self.checkHttpState(hostInfo);
                        }
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.stopHttp = function (hostInfo) {
                    self.onStopHttp({
                        hostInfo: hostInfo,
                        callback: function () {
                            self.checkHttpState(hostInfo);
                        }
                    });
                };

            }],
        bindings: {
            hostList: '<',
            distributionList: '<',
            hostCategory: '<',
            isOperationInProgress: '=',
            onAddNode: '&',
            onHostClicked: '&',
            onCheckHttpState: '&',
            onStartHttp: '&',
            onStopHttp: '&',
            onGetNodeCount: '&',
            onUninstallHost: '&',
            onReinstallSentinel: '&',
            onInstallHttp: '&',
            onCheckHttpInstall: '&'
        }
    });
