/*global angular*/
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-host-management').
    component('bitcraftHostManagement', {
        templateUrl: 'js/host-management/host-management.template.html',
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement', 'ServerConfig',
            function hostManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement,
                ServerConfig
            ) {
                var self = this;

                /** @type {SystemConfig} */
                self.config = {};
                self.hostCategory = 'http';
                self.localAddress = '';
                self.sentinelPort = '';
                self.dns = '';
                self.uploadNewSentinel = false;
                self.sentinelFile = null;

                self.installHost = function () {
                    if (self.uploadNewSentinel) {
                        angular.element('#sentinel-file-select').trigger('click');
                    } else {
                        self.addHost();
                    }
                };

                /**
                 * @param {File} file
                 */
                self.sentinelFileSelected = function (file) {
                    if (file) {
                        self.addHost(file);
                    }
                };

                self.updateDnsPrefill = function () {
                    if (self.hostCategory === 'http') {
                        self.dns = self.config.hostManagement.httpDNS;
                    } else if (self.hostCategory === 'agent') {
                        self.dns = self.config.hostManagement.agentDNS;
                    } else if (self.hostCategory === 'router') {
                        self.dns = self.config.hostManagement.routerDNS;
                    }
                };

                ServerConfig.getCurrentConfig().then(function (data) {
                    self.config = data;
                    self.updateDnsPrefill();
                    self.sentinelPort = self.config.hostManagement.sentinelPort
                }, function (resp) {
                    $log.debug('failed to retrieve config from server. (' + resp.data + ')');
                });

                /**
                 * @param {File} [sentinelFile]
                 */
                self.addHost = function (sentinelFile) {
                    /** @type {HostInfo} */
                    var newHost = {
                        _id: undefined,
                        category: self.hostCategory,
                        localAddress: self.localAddress,
                        dns: self.dns,
                        sentinelPort: parseInt(self.sentinelPort, 10)
                    };

                    Notification.info({
                        message: 'Deploying sentinel...'
                    });
                    DeploymentManagement.installHost(newHost, sentinelFile).then(function () {
                        Notification.success({
                            message: 'Successfully added new host with address "' + self.localAddress + '"'
                        });

                        // reset the text boxes
                        self.localAddress = '';
                        self.updateDnsPrefill();
                        self.sentinelPort = self.config.hostManagement.sentinelPort;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to add new host with address ' +
                            self.localAddress + ' (' + resp.statusText + ': ' + resp.data + ')'
                        });
                    });
                };

            }
        ]
    });

/**
 * @typedef {Object} HostInfo
 * @property {ObjectID|string} _id
 * @property {string} category
 * @property {string} localAddress
 * @property {string} dns
 * @property {number} sentinelPort
 */

/**
 * @typedef {Object} HostManagementConfig
 * @property {string} httpDNS
 * @property {string} agentDNS
 * @property {string} routerDNS
 * @property {number} sentinelPort
 */

/**
 * @typedef {Object} NodeManagementConfig
 * @property {number} tcpPort
 * @property {number} udpPort
 * @property {string} adminHost
 * @property {number} adminPort
 * @property {JSON} customData
 */

/**
 * @typedef {Object} SystemConfig
 * @property {HostManagementConfig} hostManagement
 * @property {NodeManagementConfig} nodeManagement
 */
